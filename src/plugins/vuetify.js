import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';

Vue.use(Vuetify);

export default new Vuetify({
    theme: {
        themes: {
            light: {
                primary: '#f44336',
                secondary: '#ffeb3b',
                accent: '#ffeb3b',
            },
        },
    },
});