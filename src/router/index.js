import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Locations from '../views/Locations'
import Unlock from "@/views/Unlock";
import Content from "@/views/Content";

Vue.use(VueRouter)

const routes = [
  {
    path: '//',
    name: 'Accueil',
    component: Home,

    meta: {
      icon: "mdi-home"
    }
  },

  {
    path: '/about',
    name: 'About',
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue'),

    meta: {
      hidden: true
    }
  },

  {
    name: 'Ligne',
    path: '/ligne/:track',
    component: Locations,
    props: true,

    meta: {
      hidden: true
    }
  },

  {
    name: 'Favoris',
    path: '/favoris',
    component: Locations,
    props: {
      favorite: true,
      errorMessage: "Aucun favori enregistré"
    },

    meta: {
      icon: "mdi-star"
    }
  },

  {
    name: 'Arrêts découverts',
    path: '/decouverts',
    component: Locations,
    props: {
      discovered: true,
      errorMessage: "Vous n'avez découvert aucun arrêt"
    },

    meta: {
      icon: "mdi-eye"
    }
  },

  {
    name: 'Unlock',
    path: "/u/:hash",
    component: Unlock,
    props: true,
    meta: {
      hidden: true
    }
  },

  {
    name: 'Content',
    path: "/arret/:slug",
    component: Content,
    props: true,
    meta: {
      hidden: true
    }

  }
]

const router = new VueRouter({
  routes
})

export default router
