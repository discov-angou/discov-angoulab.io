module.exports=[
  {
    name: 'Franquin',
    track: 'A',
    img: '/images/franquin/arret.webp',
    __content: '\n' +
      '\n' +
      '# La gaffe de Franquin\n' +
      '\n' +
      "L'espace Franquin a été nommé d'après l'auteur éponyme à sa mort, en 1997. Il a été le premier auteur à recevoir le grand prix de la ville d'Angoulême, lors du festival de la BD 1974. \n" +
      '\n' +
      "Non loin de là, à 2 minutes à pieds, au 50 rue Hergé, on peut voir deux personnages d'une de ses oeuvres : Gaston Lagaffe. \n" +
      '\n' +
      '![](/images/franquin/fresque.png.webp)\n' +
      '![](/images/franquin/itineraire.png.webp)\n' +
      '\n' +
      `À l'origine, Gaston était un personnage "bouche-trou" du magazine Spirou. Un chômeur que quelqu'un à la rédaction avait engagé, on ne sait pas trop pourquoi. En fait, Franquin ne savait pas vraiment quoi faire de ce personnage.\n` +
      '\n' +
      '![](/images/franquin/gaston.png.webp)\n' +
      '\n' +
      `Voici l'occasion de (re)découvrir une planche du premier hors série de Gaston : "Fou du bus"\n` +
      '\n' +
      '![](/images/franquin/1.png.webp)\n' +
      '![](/images/franquin/2.png.webp)\n' +
      '![](/images/franquin/3.png.webp)\n' +
      '![](/images/franquin/4.png.webp)\n' +
      '![](/images/franquin/5.png.webp)\n' +
      '![](/images/franquin/6.png.webp)\n' +
      '![](/images/franquin/7.png.webp)\n' +
      '![](/images/franquin/8.png.webp)\n',
    content: '<h1 id="la-gaffe-de-franquin">La gaffe de Franquin</h1>\n' +
      '<p>L&#39;espace Franquin a été nommé d&#39;après l&#39;auteur éponyme à sa mort, en 1997. Il a été le premier auteur à recevoir le grand prix de la ville d&#39;Angoulême, lors du festival de la BD 1974. </p>\n' +
      '<p>Non loin de là, à 2 minutes à pieds, au 50 rue Hergé, on peut voir deux personnages d&#39;une de ses oeuvres : Gaston Lagaffe. </p>\n' +
      '<p><img src="/images/franquin/fresque.png.webp" alt="">\n' +
      '<img src="/images/franquin/itineraire.png.webp" alt=""></p>\n' +
      '<p>À l&#39;origine, Gaston était un personnage &quot;bouche-trou&quot; du magazine Spirou. Un chômeur que quelqu&#39;un à la rédaction avait engagé, on ne sait pas trop pourquoi. En fait, Franquin ne savait pas vraiment quoi faire de ce personnage.</p>\n' +
      '<p><img src="/images/franquin/gaston.png.webp" alt=""></p>\n' +
      '<p>Voici l&#39;occasion de (re)découvrir une planche du premier hors série de Gaston : &quot;Fou du bus&quot;</p>\n' +
      '<p><img src="/images/franquin/1.png.webp" alt="">\n' +
      '<img src="/images/franquin/2.png.webp" alt="">\n' +
      '<img src="/images/franquin/3.png.webp" alt="">\n' +
      '<img src="/images/franquin/4.png.webp" alt="">\n' +
      '<img src="/images/franquin/5.png.webp" alt="">\n' +
      '<img src="/images/franquin/6.png.webp" alt="">\n' +
      '<img src="/images/franquin/7.png.webp" alt="">\n' +
      '<img src="/images/franquin/8.png.webp" alt=""></p>\n',
    hash: 'ca7162771bd0c90bfa9490a54dc293ca'
  },
  {
    name: 'Gare SNCF',
    track: 'A',
    img: '/images/gare-sncf/gare-angouleme.jpg.webp',
    __content: '\n' +
      '\n' +
      "# New-York s'est appelé Angoulême.\n" +
      '\n' +
      "## L'histoire\n" +
      '\n' +
      '\n' +
      'En 1524, dans le sillage des expéditions de Christophe Colomb, une expédition française dirigée par le navigateur Giovanni da Verrazano, découvrait une baie inconnue au Nord des États-Unis, site de la future ville de New York.\n' +
      '\n' +
      "Il la baptisa « Nouvelle-Angoulême » en l’honneur de son roi, François 1er. (également surnommé François d'Angoulême)\n" +
      '\n' +
      'Il envoie son carnet de bord à son roi, mais François Ier est fait prisonnier par Charles Quint en 1525 et il ne recevra jamais le document. Les vicissitudes de l’Histoire changèrent ensuite le nom de cette contrée en « Nouvelle-Amsterdam » en 1612, puis en New York.\n' +
      '\n' +
      '\n' +
      '## La fresque\n' +
      '\n' +
      `Pour rendre hommage à cette partie de l'histoire, l'artiste Nicolas de Crécy conçoit en 2001 la plus grande fresque de la ville d'Angoulême "New York sur Charente", mesurant 10 étages de haut, sur la façade d'un immeuble.\n` +
      '\n' +
      '\n' +
      '![](/images/gare-sncf/fresque-mur.jpg.webp)\n' +
      '\n' +
      '\n' +
      'Étant à quelques pas de la gare, si vous voulez aller contempler la fresque, il vous suffit de remonter sur une cinquantaine de mètres la rue vers le centre ville et de tourner à gauche au bâtiment bleu pâle, dans la rue de la Grand Font.\n',
    content: '<h1 id="new-york-sest-appelé-angoulême">New-York s&#39;est appelé Angoulême.</h1>\n' +
      '<h2 id="lhistoire">L&#39;histoire</h2>\n' +
      '<p>En 1524, dans le sillage des expéditions de Christophe Colomb, une expédition française dirigée par le navigateur Giovanni da Verrazano, découvrait une baie inconnue au Nord des États-Unis, site de la future ville de New York.</p>\n' +
      '<p>Il la baptisa « Nouvelle-Angoulême » en l’honneur de son roi, François 1er. (également surnommé François d&#39;Angoulême)</p>\n' +
      '<p>Il envoie son carnet de bord à son roi, mais François Ier est fait prisonnier par Charles Quint en 1525 et il ne recevra jamais le document. Les vicissitudes de l’Histoire changèrent ensuite le nom de cette contrée en « Nouvelle-Amsterdam » en 1612, puis en New York.</p>\n' +
      '<h2 id="la-fresque">La fresque</h2>\n' +
      '<p>Pour rendre hommage à cette partie de l&#39;histoire, l&#39;artiste Nicolas de Crécy conçoit en 2001 la plus grande fresque de la ville d&#39;Angoulême &quot;New York sur Charente&quot;, mesurant 10 étages de haut, sur la façade d&#39;un immeuble.</p>\n' +
      '<p><img src="/images/gare-sncf/fresque-mur.jpg.webp" alt=""></p>\n' +
      '<p>Étant à quelques pas de la gare, si vous voulez aller contempler la fresque, il vous suffit de remonter sur une cinquantaine de mètres la rue vers le centre ville et de tourner à gauche au bâtiment bleu pâle, dans la rue de la Grand Font.</p>\n',
    hash: 'bf36d51cde9ecf9b4652298a46a75201'
  },
  {
    name: 'Hôtel de Ville',
    track: 'A',
    img: '/images/hotel-de-ville/hotelDeVille.png.webp',
    __content: '\n' +
      '\n' +
      '# La jambe de Clovis\n' +
      '\n' +
      "Non loin de cet arrêt, sur la deuxième enceinte des remparts, une tour s'est vu décorée d'une jambe assez grossièrement sculptée.\n" +
      "Celle-ci represente **la jambe de Clovis**, le roi des Francs de l'an 481 à 511.\n" +
      '\n' +
      '![](/images/hotel-de-ville/jambeClovis.png.webp)\n' +
      '\n' +
      '## Où la trouver?\n' +
      '\n' +
      'Cette tour est située **rue Louis Debrandes** sur un petit chemin piéton longeant les remparts.\n' +
      '\n' +
      '![](/images/hotel-de-ville/itineraireHDV_Clovis.png.webp)\n' +
      '\n' +
      '## Son histoire\n' +
      '\n' +
      "En 507, l'armée des Francs et celle des Visigoths font trembler les terres du Milieu _de la France_, on parle ici de la bataille de Vouillé qui marqua une étape importante de la conquête de la France par Clovis.\n" +
      "Après la défaite des Visigoths, ces derniers se refugièrent en haut des remparts dîtes imprenables d'Angoulême que Clovis attaqua sans succès avant de partir pour Bordeaux.\n" +
      "Mais c'est en 508, lors d'un deuxième siège de Clovis que celui-ci parvint à prendre miraculeusement la ville.\n" +
      '\n' +
      'Suite à cette victoire pendant une procession, des murailles de la ville se seraient ecrasées devant lui, le blessant à la jambe.\n' +
      "Ainsi cette scuplture de jambe faite au XIIème siècle rend hommage à cette histoire en indiquant l'endroit où l'incident s'est produit.",
    content: '<h1 id="la-jambe-de-clovis">La jambe de Clovis</h1>\n' +
      '<p>Non loin de cet arrêt, sur la deuxième enceinte des remparts, une tour s&#39;est vu décorée d&#39;une jambe assez grossièrement sculptée.\n' +
      'Celle-ci represente <strong>la jambe de Clovis</strong>, le roi des Francs de l&#39;an 481 à 511.</p>\n' +
      '<p><img src="/images/hotel-de-ville/jambeClovis.png.webp" alt=""></p>\n' +
      '<h2 id="où-la-trouver">Où la trouver?</h2>\n' +
      '<p>Cette tour est située <strong>rue Louis Debrandes</strong> sur un petit chemin piéton longeant les remparts.</p>\n' +
      '<p><img src="/images/hotel-de-ville/itineraireHDV_Clovis.png.webp" alt=""></p>\n' +
      '<h2 id="son-histoire">Son histoire</h2>\n' +
      '<p>En 507, l&#39;armée des Francs et celle des Visigoths font trembler les terres du Milieu <em>de la France</em>, on parle ici de la bataille de Vouillé qui marqua une étape importante de la conquête de la France par Clovis.\n' +
      'Après la défaite des Visigoths, ces derniers se refugièrent en haut des remparts dîtes imprenables d&#39;Angoulême que Clovis attaqua sans succès avant de partir pour Bordeaux.\n' +
      'Mais c&#39;est en 508, lors d&#39;un deuxième siège de Clovis que celui-ci parvint à prendre miraculeusement la ville.</p>\n' +
      '<p>Suite à cette victoire pendant une procession, des murailles de la ville se seraient ecrasées devant lui, le blessant à la jambe.\n' +
      'Ainsi cette scuplture de jambe faite au XIIème siècle rend hommage à cette histoire en indiquant l&#39;endroit où l&#39;incident s&#39;est produit.</p>\n',
    hash: '29f40cfb049fc5e16771954886eaf785'
  },
  {
    name: 'Rabion',
    track: 'A',
    img: '/images/rabion/rabion.webp',
    __content: '\n' +
      '\n' +
      "# Le tramway d'Angoulême\n" +
      '\n' +
      '_Angoulême, 1922_\n' +
      '\n' +
      '<div style="text-align: justify !important">\n' +
      "Droit comme un piquet, Charles attend. Il expire une fumée nauséabonde et il attend. Lorsque son cylindre de papier arrive à terme, il l'écrase au sol et il attend.\n" +
      '\n' +
      "Sa casquette vissée sur le crâne le protège du soleil de fin d'après midi et lui confère une certaine élégance ; ses chaussures pourries par un lubrifiant mécanique entâchent discrètement celle-ci.\n" +
      '\n' +
      "Un grincement retentit, il attend. De la fumée s'échappe, il attend. Quelques personnes s'affolent, il attend. Un pas en avant, il monte à bord du tramway.\n" +
      '\n' +
      "Charles jette un coup d'œil à l'avant, à travers le rétroviseur il aperçoit un conducteur. Un regard à l'arrière, le conducteur observe Charles. L'engin démarre, les passagers s'accrochent. Dans le miroir, le conducteur épie l'ouvrier, son regard l'y attend. Ce n'est pas la première fois. Ils se connaissent, c'est leur petit jeu ; ils ont servi ensemble.\n" +
      '\n' +
      "Ce que Charles avait, les autres soldats ne l'avaient pas, en tout cas à ses yeux. Ceux-là mêmes qui sondent l'ouvrier à intervalles réguliers. À l'heure de rentrer chez eux, ils ne se sont plus jamais parlé. Mais chaque jour Charles attend et ils retrouvent leur jeu.\n" +
      '\n' +
      "L'un des regards faillit, une porte s'ouvre ; l'ouvrier descend. La machine démarre et laisse là l'élégant pouilleux. Il rentre chez lui en sa propre compagnie et retournera attendre demain et le lendemain encore, à ce même arrêt où chaque année il attend.\n" +
      '\n' +
      '<br>\n' +
      '<br>\n' +
      '\n' +
      '_Ce texte est une oeuvre de fiction, toute ressemblance avec des personnes réelles serait fortuite._\n' +
      '\n' +
      "En réalité, la rue de Rabion qui donne son nom à l'arrêt ne porte le nom d'aucune personnalité notable de la région. Il s'agit juste d'un nom de famille commun dans la région.\n" +
      '</div>',
    content: '<h1 id="le-tramway-dangoulême">Le tramway d&#39;Angoulême</h1>\n' +
      '<p><em>Angoulême, 1922</em></p>\n' +
      '<div style="text-align: justify !important">\n' +
      "Droit comme un piquet, Charles attend. Il expire une fumée nauséabonde et il attend. Lorsque son cylindre de papier arrive à terme, il l'écrase au sol et il attend.\n" +
      '\n' +
      '<p>Sa casquette vissée sur le crâne le protège du soleil de fin d&#39;après midi et lui confère une certaine élégance ; ses chaussures pourries par un lubrifiant mécanique entâchent discrètement celle-ci.</p>\n' +
      '<p>Un grincement retentit, il attend. De la fumée s&#39;échappe, il attend. Quelques personnes s&#39;affolent, il attend. Un pas en avant, il monte à bord du tramway.</p>\n' +
      '<p>Charles jette un coup d&#39;œil à l&#39;avant, à travers le rétroviseur il aperçoit un conducteur. Un regard à l&#39;arrière, le conducteur observe Charles. L&#39;engin démarre, les passagers s&#39;accrochent. Dans le miroir, le conducteur épie l&#39;ouvrier, son regard l&#39;y attend. Ce n&#39;est pas la première fois. Ils se connaissent, c&#39;est leur petit jeu ; ils ont servi ensemble.</p>\n' +
      '<p>Ce que Charles avait, les autres soldats ne l&#39;avaient pas, en tout cas à ses yeux. Ceux-là mêmes qui sondent l&#39;ouvrier à intervalles réguliers. À l&#39;heure de rentrer chez eux, ils ne se sont plus jamais parlé. Mais chaque jour Charles attend et ils retrouvent leur jeu.</p>\n' +
      '<p>L&#39;un des regards faillit, une porte s&#39;ouvre ; l&#39;ouvrier descend. La machine démarre et laisse là l&#39;élégant pouilleux. Il rentre chez lui en sa propre compagnie et retournera attendre demain et le lendemain encore, à ce même arrêt où chaque année il attend.</p>\n' +
      '<br>\n' +
      '<br>\n' +
      '\n' +
      '<p><em>Ce texte est une oeuvre de fiction, toute ressemblance avec des personnes réelles serait fortuite.</em></p>\n' +
      '<p>En réalité, la rue de Rabion qui donne son nom à l&#39;arrêt ne porte le nom d&#39;aucune personnalité notable de la région. Il s&#39;agit juste d&#39;un nom de famille commun dans la région.</p>\n' +
      '</div>',
    hash: 'f3a8ab75355e99c99d57f70a7ecfb8a9'
  }
]