import Vue from 'vue'
import App from './App.vue'
import router from './router'
import vuetify from './plugins/vuetify'
import storage from '@/storage'
import '@/assets/main.sass'

Vue.config.productionTip = false

const app = new Vue({
  router,
  vuetify,
  render: h => h(App),

  data: {
    locations: require('@/locations.js')
  }
}).$mount('#app')