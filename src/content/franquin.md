---
name: Franquin
track: "A"
img: "/images/franquin/arret.webp"
---

# La gaffe de Franquin

L'espace Franquin a été nommé d'après l'auteur éponyme à sa mort, en 1997. Il a été le premier auteur à recevoir le grand prix de la ville d'Angoulême, lors du festival de la BD 1974. 

Non loin de là, à 2 minutes à pieds, au 50 rue Hergé, on peut voir deux personnages d'une de ses oeuvres : Gaston Lagaffe. 

![](/images/franquin/fresque.png.webp)
![](/images/franquin/itineraire.png.webp)

À l'origine, Gaston était un personnage "bouche-trou" du magazine Spirou. Un chômeur que quelqu'un à la rédaction avait engagé, on ne sait pas trop pourquoi. En fait, Franquin ne savait pas vraiment quoi faire de ce personnage.

![](/images/franquin/gaston.png.webp)

Voici l'occasion de (re)découvrir une planche du premier hors série de Gaston : "Fou du bus"

![](/images/franquin/1.png.webp)
![](/images/franquin/2.png.webp)
![](/images/franquin/3.png.webp)
![](/images/franquin/4.png.webp)
![](/images/franquin/5.png.webp)
![](/images/franquin/6.png.webp)
![](/images/franquin/7.png.webp)
![](/images/franquin/8.png.webp)
