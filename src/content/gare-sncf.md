---
name: Gare SNCF
track: "A"
img: "/images/gare-sncf/gare-angouleme.jpg.webp"
---

# New-York s'est appelé Angoulême.

## L'histoire


En 1524, dans le sillage des expéditions de Christophe Colomb, une expédition française dirigée par le navigateur Giovanni da Verrazano, découvrait une baie inconnue au Nord des États-Unis, site de la future ville de New York.

Il la baptisa « Nouvelle-Angoulême » en l’honneur de son roi, François 1er. (également surnommé François d'Angoulême)

Il envoie son carnet de bord à son roi, mais François Ier est fait prisonnier par Charles Quint en 1525 et il ne recevra jamais le document. Les vicissitudes de l’Histoire changèrent ensuite le nom de cette contrée en « Nouvelle-Amsterdam » en 1612, puis en New York.


## La fresque

Pour rendre hommage à cette partie de l'histoire, l'artiste Nicolas de Crécy conçoit en 2001 la plus grande fresque de la ville d'Angoulême "New York sur Charente", mesurant 10 étages de haut, sur la façade d'un immeuble.


![](/images/gare-sncf/fresque-mur.jpg.webp)


Étant à quelques pas de la gare, si vous voulez aller contempler la fresque, il vous suffit de remonter sur une cinquantaine de mètres la rue vers le centre ville et de tourner à gauche au bâtiment bleu pâle, dans la rue de la Grand Font.
