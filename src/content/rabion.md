---
name: Rabion
track: "A"
img: "/images/rabion/rabion.webp"
---

# Le tramway d'Angoulême

_Angoulême, 1922_

<div style="text-align: justify !important">
Droit comme un piquet, Charles attend. Il expire une fumée nauséabonde et il attend. Lorsque son cylindre de papier arrive à terme, il l'écrase au sol et il attend.

Sa casquette vissée sur le crâne le protège du soleil de fin d'après midi et lui confère une certaine élégance ; ses chaussures pourries par un lubrifiant mécanique entâchent discrètement celle-ci.

Un grincement retentit, il attend. De la fumée s'échappe, il attend. Quelques personnes s'affolent, il attend. Un pas en avant, il monte à bord du tramway.

Charles jette un coup d'œil à l'avant, à travers le rétroviseur il aperçoit un conducteur. Un regard à l'arrière, le conducteur observe Charles. L'engin démarre, les passagers s'accrochent. Dans le miroir, le conducteur épie l'ouvrier, son regard l'y attend. Ce n'est pas la première fois. Ils se connaissent, c'est leur petit jeu ; ils ont servi ensemble.

Ce que Charles avait, les autres soldats ne l'avaient pas, en tout cas à ses yeux. Ceux-là mêmes qui sondent l'ouvrier à intervalles réguliers. À l'heure de rentrer chez eux, ils ne se sont plus jamais parlé. Mais chaque jour Charles attend et ils retrouvent leur jeu.

L'un des regards faillit, une porte s'ouvre ; l'ouvrier descend. La machine démarre et laisse là l'élégant pouilleux. Il rentre chez lui en sa propre compagnie et retournera attendre demain et le lendemain encore, à ce même arrêt où chaque année il attend.

<br>
<br>

_Ce texte est une oeuvre de fiction, toute ressemblance avec des personnes réelles serait fortuite._

En réalité, la rue de Rabion qui donne son nom à l'arrêt ne porte le nom d'aucune personnalité notable de la région. Il s'agit juste d'un nom de famille commun dans la région.
</div>