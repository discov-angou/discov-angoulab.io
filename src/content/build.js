const fs = require('fs')
const path = require('path')
const marked = require('marked')
const front = require('yaml-front-matter')
const util = require("util")
const crypto = require("crypto")
const qr = require("qrcode")

const baseUrl = "http://discov-angou.gitlab.io/#/u/"

// 1. Construit un objet JS
// 2. Lis le contenu d'un fichier et l'ajoute à l'objet
// 3. Créer un module JS exportant l'objet

const objs = []

const files =
    fs.readdirSync(__dirname, {withFileTypes: true})
        .filter(dirent => dirent.isFile() && path.extname(dirent.name) === '.md')
        .map(dirent => path.join(__dirname,dirent.name));

files.forEach(file => {
    let data = fs.readFileSync(file, 'utf8');
    let obj = front.loadFront(data);
    obj.content = marked(obj.__content);

    obj.hash = crypto.createHash('md5').update(data).digest('hex');

    // Generate QR Code
    qr.toFile(
        `qrcodes/${obj.track} - ${obj.name}.png`,
        [{data: baseUrl+obj.hash}]
    )

    objs.push(obj)
});


fs.writeFileSync('src/locations.js', "module.exports="+util.inspect(objs));