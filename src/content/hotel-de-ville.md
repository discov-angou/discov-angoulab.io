---
name: Hôtel de Ville
track: "A"
img: "/images/hotel-de-ville/hotelDeVille.png.webp"
---

# La jambe de Clovis

Non loin de cet arrêt, sur la deuxième enceinte des remparts, une tour s'est vu décorée d'une jambe assez grossièrement sculptée.
Celle-ci represente **la jambe de Clovis**, le roi des Francs de l'an 481 à 511.

![](/images/hotel-de-ville/jambeClovis.png.webp)

## Où la trouver?

Cette tour est située **rue Louis Debrandes** sur un petit chemin piéton longeant les remparts.

![](/images/hotel-de-ville/itineraireHDV_Clovis.png.webp)

## Son histoire

En 507, l'armée des Francs et celle des Visigoths font trembler les terres du Milieu _de la France_, on parle ici de la bataille de Vouillé qui marqua une étape importante de la conquête de la France par Clovis.
Après la défaite des Visigoths, ces derniers se refugièrent en haut des remparts dîtes imprenables d'Angoulême que Clovis attaqua sans succès avant de partir pour Bordeaux.
Mais c'est en 508, lors d'un deuxième siège de Clovis que celui-ci parvint à prendre miraculeusement la ville.

Suite à cette victoire pendant une procession, des murailles de la ville se seraient ecrasées devant lui, le blessant à la jambe.
Ainsi cette scuplture de jambe faite au XIIème siècle rend hommage à cette histoire en indiquant l'endroit où l'incident s'est produit.