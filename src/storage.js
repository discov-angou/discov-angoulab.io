const slugify = require('@/slugify')

const key = "discov-angou-data";

module.exports = {

    save(locations) {
        let store = locations.map(loc => ({
            name: loc.name,
            discovered: loc.discovered,
            favorite: loc.favorite
        }));
        window.localStorage.setItem(key, JSON.stringify(store));
    },

    load(locations) {
        let store = window.localStorage.getItem(key);
        console.log(store)

        if (!store) {
            this.save(locations)
            return this.load(locations)
        }

        store = JSON.parse(store);

        locations.forEach(loc => {
            let stored = store.find(l => l.name === loc.name);
            if (stored) {
                loc.name = stored.name;
                loc.discovered = stored.discovered;
                loc.favorite = stored.favorite;
            }
        });

        return locations
    }
}