# Discov'Angou
To do List

# Rappel du projet
Discov’Angou est une installation dont l’objectif est de transformer les abri-bus, lieux d’attente et d’ennui, en lieux d’apprentissage et de divertissement à travers un jeu de piste.
Les utilisateurs devront trouver des QR codes cachés dans les différents abri-bus afin de débloquer un contenu : des informations sur le lieu où ils se trouvent, une anecdote, une histoire ou une bande dessinée. Les contenus de chaque abri-bus sont stockés sur le téléphone, l’objectif est de débloquer tous les arrêts.

# Todo

## Général
- [X] Définition d’un feeling/direction pour l’installation
- [ ] Impression des éléments
- [ ] Installation

## Graphisme
- [X] Identité visuelle
- [X] Visuels pour les stickers
- [X] UI Web app

## Design
- [ ] Définition de la cible
- [X] Choix des arrêts et contenus
- [X] Choix emplacement stickers et aménagement
- [X] Création des contenus
  - [X] Cathédrale
  - [X] Gare
  - [X] Mairie
  - [X] Rabion